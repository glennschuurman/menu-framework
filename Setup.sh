#!/bin/bash
# if [ "$EUID" -ne 0 ]
#   then echo "Please run as root"
#   exit
# fi

prefix='OPTION="'
suffix='"'

OPTIONS=()

source utils/utils

for f in $(ls modules/ | grep .sh | sort -n); do
    option=`cat modules/$f | grep "OPTION="`
    option=${option#"$prefix"} 
    option=${option%"$suffix"}
    index="`basename "$f" | cut -d'.' -f 1`"
    OPTIONS+=($index "$option" off)
done

function get_root() {
    echo $sudoPassword | sudo -S echo "Gaining root"
}

function menu(){
	BACKTITLE="Menu"
	TITLE="Options"
	MENU="Select all desired options with the space bar:"

    CHOICES=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --checklist "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)
    
    if [ $? -eq 0 ]
    then
        if [ -z "$CHOICES" ] ; then
            menu
        fi
        for choice in $CHOICES ; do
            modules/$choice.*.sh
        done
    else
        echo cancel selected
    fi
}

read -s -p "Please enter the sudo password:" sudoPassword
get_root

if [ ! -f "/usr/bin/dialog" ] ; then
    sudo apt install dialog -y
fi
if [ ! -f "/usr/bin/dialog" ] ; then
    sudo dpkg -i resources/dialog.deb
fi

if [ ! -f "/usr/bin/dialog" ] ; then
    echo "can't install dialog"
    exit 0
fi

menu
exec 3>&1 
response=$(dialog --title "Information"  --yesno "Finished setting up your system.\n To activate all changes the system must reboot. \n Do you want to reboot now? " 0 0 2>&1 1>&3)
InfoCorrect=$?;
exec 3>&-;
if [ $InfoCorrect == "0" ]; then
    sudo reboot
else
    clear
    exit 0
fi
